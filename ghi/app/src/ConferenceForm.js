import React, { useEffect, useState } from "react";

function ConferenceForm(props) {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState("");
  const [starts, setStarts] = useState("");
  const [ends, setEnds] = useState("");
  const [description, setDescription] = useState("");
  const [max_presentations, setMaxPresentations] = useState("");
  const [max_attendees, setMaxAttendees] = useState("");
  const [location, setLocation] = useState("");



  const handleNameChange = (e) => {
    const value = e.target.value;
    setName(value);
  };
  const handleStartsChange = (e) => {
    const value = e.target.value;
    setStarts(value);
  };
  const handleEndsCountChange = (e) => {
    const value = e.target.value;
    setEnds(value);
  };
  const handleDescriptionChange = (e) => {
    const value = e.target.value;
    setDescription(value);
  };
  const handleMaxPresentationsChange = (e) => {
    const value = e.target.value;
    setMaxPresentations(value);
  };
  const handleMaxAttendeesChange = (e) => {
    const value = e.target.value;
    setMaxAttendees(value);
  };const handleLocationChange = (e) => {
    const value = e.target.value;
    setLocation(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = max_presentations;
    data.max_attendees = max_attendees;
    data.location = location;

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName("");
      setStarts("");
      setEnds("");
      setDescription("");
      setMaxPresentations("");
      setMaxAttendees("");
      setLocation("");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Conference</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange}placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={starts} onChange={handleStartsChange}placeholder="start date" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="room_count">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input value={ends} onChange={handleEndsCountChange} placeholder="City" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="city">End Date</label>
            </div>
            <div className="form-floating mb-3">
              <input value={description} onChange={handleDescriptionChange} placeholder="Description" required type="textarea" name="description" id="description" className="form-control" />
              <label htmlFor="city">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input value={max_presentations} onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="ends" className="form-control" />
              <label htmlFor="city">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input value={max_attendees} onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="ends" className="form-control" />
              <label htmlFor="city">Max attendees</label>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a Conference location?</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button onClick= {handleSubmit} className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;